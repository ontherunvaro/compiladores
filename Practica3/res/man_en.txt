calculator v0.1

USAGE
    calculator [filename]
Arguments:
    filename    file to load

This program has two operating modes:
    1. File loading
    2. Interactive prompt
Execution will default to mode 2 if no filename is provided, and 1 otherwise.

DATA TYPES
This program uses a 'double' floating point representation for all numeric values. Precision loss 
may occur.

FILE LOADING
Commands should be in separate lines. Empty lines will be ignored. By default, file loading prints 
all variables in memory at the end of the execution. You can print variables at any time by 
inputting a 'print' command.
Files must ALWAYS end with a newline.
Execution will fallback to interactive prompt at the end of the file.

INTERACTIVE PROMPT
The interactive prompt will wait for user commands and act accordingly. To input a command, type it 
and press ENTER.

COMMANDS
A command may be any of the following:
    - Mathematical operations
    - Reserved words (with optional parameters)

COMMENTS 
Any content in a line after a '#' will be ignored. Similarly, lines beginning with '#' will be 
completely ignored.

MATHEMATICAL OPERATIONS
This program supports various operations:
    
    '+'             sum
    '-'             subtraction
    '*'             multiplication
    '/'             division
    '**'            power

The operand '-' used before an expression signifies negation. Expressions can be combined freely and
can be grouped using parentheses ( '(' and ')').

VARIABLES
The calculator supports basic variable declaration and usage:
    a = 4       declares variable "a" with value 4.0
    b = 4*a     declares variable "b" with value 4.0*4.0=16.0
Variables need to be declared prior to use, for example, the expression:
    a= 4*b
is not valid unless 'b' has been given a value before.
Additionally, the program will always start with the following constants in memory:
    
    Constant    Value(truncated)        Meaning
    pi          3.141593                The number Pi
    e           2.718282                Euler number

Variable declaration may be chained or used mid-expression: both
    a=b=c=4
and
    a=4*b=2
are valid commands.

FUNCTIONS
The program has several built-in mathematical functions. To use a function, input:
    function(expression)
where 'function' is a function name. For example:
    print floor(cos(4))
will print the integer part of the cosine of 4.0rad.
The following functions are available:

    Function    Meaning
    acos        arc cosine
    asin        arc sine
    atan        arc tangent
    cos         cosine
    cosh        hyperbolic cosine
    sin         sine
    sinh        hyperbolic sine
    tanh        hyperbolic tangent
    exp         base-e exponential function
    ln          natural logarithm
    log         base-10 logarithm
    sqrt        square root
    ceil        nearest integer, rounded up
    floor       nearest integer, truncated down
    abs         absolute value

All trigonometric functions expect a radian input.

RESERVED WORDS
The following 'special' commands are available:
    
    exit                    Ends execution
    help, ?                 Prints this help
    init, reset             Reset variable table (clear all user-set variables)
    print [expression]      Explained in the next section

THE 'PRINT' RESERVED WORD
This program does not output any value to the user unless it is requested. To do this, input:
    print expression
to print the value of an expression. For example, 'print a' will print the value of the variable 'a'
and 'print b=4*a' will print the value of '4*a' and assign it to 'b'.
Inputting just 'print' without any expression after will print all currently declared variables and 
their values. This command is always executed after a file is completely parsed.

