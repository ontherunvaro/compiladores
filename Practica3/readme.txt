calculator v0.1
Ficheros incluidos en la entrega:
	.
	├── lib					
	│   └── uthash.h		Tabla hash (para tabla de simbolos)
	├── res					
	│   ├── man_en.txt		Manual (inglés)
	│   └── man_es.txt		Manual (español)
	├── src					
	│   ├── lex.l			Analizador léxico
	│   ├── run.c           Archivo principal de ejecucion
	│   ├── symtab.c		Tabla de simbolos
	│   ├── symtab.h		Tabla de simbolos (cabecera)
	│   ├── syn.y			Analizador sintáctico
	│   ├── util.c			Utilidades
	│   └── util.h			Utilidades (cabecera)
	├── test				
	│   ├── test1.dat		Fichero de prueba
	│   ├── test2.dat		Fichero de prueba
	│   ├── test3.dat		Fichero de prueba
	│   └── test4.dat		Fichero de prueba
	├── makefile			
	└── readme.txt

Compilación del programa:
	$ make
Limpiar archivos generados:
	$ make clean

Ejecución:
	./calculator
Se recomienda iniciar la primera vez de la siguiente manera:
	./calculator -h
para imprimir el uso del programa y las distintas opciones.
Visualización del manual:
	./calculator -m (inglés)
	./calculator -e (español)
o bien usando el comando 'help' (inglés) o 'help es'(español) en la consola interactiva.
NOTA: los manuales también pueden ser consultados en la carpeta res/
