/*
 * util.h
 *
 * Fichero de utilidades comunes
 *
 * El manejo de líneas se lleva a cabo en este fichero
 * para que sea accesible desde las distintas partes del programa.
 */

#ifndef SRC_UTIL_H_
#define SRC_UTIL_H_

extern int readingFile;

/*
 * Devuelve linea actual
 */
int getLineCount();

/*
 * Añade una linea al contador
 */
void addLine();

/*
 * Añade n lineas al contador
 */
void addLines(int n);

void printHelp();

void printHelp_es();

#endif /* SRC_UTIL_H_ */
