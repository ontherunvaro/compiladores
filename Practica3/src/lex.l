
%{
	
	#include<stdio.h>
	#include<stdlib.h>
	#include<string.h>
	#include"symtab.h"
	#include"syn.tab.h"
	#include"util.h"
	#include<math.h>
	
	int yywrap(void);
	
	extern int readingFile;
	extern void yyerror(char *s);
		

%}


DIG [0-9]+
FLOAT {DIG}\.{DIG}
EXP e\-?{DIG}
SIN_SYMB [\+\-\*\/\=\<\>\(\)]
ID [_[:alpha:]][_[:alnum:]]*

%%
	int aux;
	symbol auxs;
	char err[256];
	
{DIG}\.?{DIG}?{EXP}? 	{yylval.fv=atof(yytext); return NUM;}
\.{DIG}{EXP}?			{yylval.fv=atof(yytext); return NUM;}

\*\*		return EXP;
\n			return '\n';
\ 			;			
{SIN_SYMB}	return yytext[0];
exit		return EXIT;
init|reset	return INIT;
print|pr	return PRINT;
help|\?		return HELP;
es			return ES;
{ID}		{
				yylval.sv.nombre=strdup(yytext);
				auxs=leeSimbolo(yytext);
				
				aux=auxs.tipo;				
				
				if(aux!=NOT_FOUND){
				yylval.sv.value=auxs.value;
				yylval.sv.tipo=auxs.tipo;
				}else{
				yylval.sv.tipo=VAR;
				yylval.sv.value.var=NAN;
				aux=VAR;
				}
							
				return aux;
			}
			
#[^\n]*$		
.			{
				sprintf(err,"Unrecognized character \'%c\', ignoring...", yytext[0]);
				yyerror(err);
			}

%%
int yywrap(void){
	//fallback a stdout si estamos en archivo
	if(readingFile==1){
		readingFile=0;
		yyset_in(stdin);
		imprimeVariables();
		printf(">");
		return 0;
	}
	return 1;
}