/*
 * symtab.h
 *
 *  Cabecera de la tabla de símbolos
 */

#ifndef SRC_SYMTAB_H_
#define SRC_SYMTAB_H_

#define NOT_FOUND 2777

typedef union {
	double var;
	double (*func)();
} union_val;

typedef struct {
	char * nombre;
	int tipo;
	union_val value;
} symbol;

void escribeSimbolo(symbol sym);

symbol leeSimbolo(char *nombre);

void imprimeVariables(void);

void inicializarTabla(void);

#endif /* SRC_SYMTAB_H_ */
