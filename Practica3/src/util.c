/*
 * util.c
 *
 * Implementación de las utilidades
 */

#include <stdio.h>

#define ES_MAN "res/man_es.txt"
#define EN_MAN "res/man_en.txt"

int lines = 1;

void addLine() {
	lines++;
}

void addLines(int n) {
	lines += n;
}

int getLineCount() {
	return lines;
}

void printFile(char * fname) {
	int c;
	FILE * file;
	file = fopen(fname, "rt");
	if (file) {
		while ((c = getc(file)) != EOF)
			putchar(c);
		fclose(file);
	}
}
void printHelp() {
	printFile(EN_MAN);
}

void printHelp_es() {
	printFile(ES_MAN);

}
