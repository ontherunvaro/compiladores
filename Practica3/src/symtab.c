/*
 * symtab.c
 *
 *  Implementación de la tabla de símbolos
 */

#include "symtab.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../lib/uthash.h"
#include "syn.tab.h"

typedef struct {
	char * nombre;
	int tipo;
	union_val value;
	UT_hash_handle hh;
} Celda;

Celda *tabla = NULL;

void add(symbol sym) {
	Celda *s;
	char * c;

	s = (Celda *) malloc(sizeof(Celda));
	c = strdup(sym.nombre);

	s->nombre = c;
	s->tipo = sym.tipo;
	s->value = sym.value;

	HASH_ADD_KEYPTR(hh, tabla, s->nombre, strlen(s->nombre), s);
}

Celda * find(char * nombre) {
	Celda *s = NULL;
	HASH_FIND_STR(tabla, nombre, s);
	return s;
}

void delete(char * nombre) {
	Celda * s = find(nombre);
	if (s != NULL)
	HASH_DEL(tabla, s);
}

void escribeSimbolo(symbol sym) {
	delete (sym.nombre);
	add(sym);
}

symbol leeSimbolo(char *nombre) {
	Celda *s = find(nombre);
	symbol ret;
	if (s != NULL) {
		ret.nombre = s->nombre;
		ret.tipo = s->tipo;
		ret.value = s->value;
	} else {
		ret.tipo = NOT_FOUND;
	}
	return ret;
}

void imprimeVariables(void) {
	Celda *s, *tmp;

	int i = 0, k = 0;

	printf("\n %-20s %-20s", "Name", "Value");

	printf("\n");
	for (i = 0; i < 49; i++) {
		printf("-");
	}
	HASH_ITER(hh, tabla, s, tmp)
	{
		if (s->tipo == VAR) {
			printf("\n %-20s %-20lf", s->nombre, s->value.var);
		}
	}
	printf("\n");
	for (i = 0; i < 49; i++) {
		printf("-");
	}
	printf("\n");
}

void inicializarTabla(void) {
	typedef struct {
		char * name;
		double value;
	} initc;
	typedef struct {
		char * name;
		double (*func)();
	} initf;

	int i = 0;
	symbol s;
	//CONSTANTES
	initc initValues[] = { "pi", M_PI, "e", M_E, 0, 0 };
	//FUNCIONES
	initf initFunctions[] = { "acos", acos, "asin", asin, "atan", atan, "cos",
			cos, "cosh", cosh, "tan", tan, "sin", sin, "sinh", sinh, "tanh",
			tanh, "exp", exp, "ln", log, "log", log10, "sqrt", sqrt, "ceil",
			ceil, "abs", fabs, "floor", floor, 0, 0 };

	s.tipo = CONST;
	for (i = 0; initValues[i].name != 0; i++) {
		s.nombre = initValues[i].name;
		s.value.var = initValues[i].value;
		escribeSimbolo(s);
	}
	s.tipo = FUNC;
	for (i = 0; initFunctions[i].name != 0; i++) {
		s.nombre = initFunctions[i].name;
		s.value.func = initFunctions[i].func;
		escribeSimbolo(s);
	}
}
