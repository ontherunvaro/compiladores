/*
 * run.c
 *
 *  Created on: 20 de nov. de 2015
 *  Fichero principal del programa
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lex.yy.h"
#include "symtab.h"
#include "syn.tab.h"
#include "util.h"

int readingFile = 0;
void init();
void printUsage();

int main(int argc, const char * argv[]) {

	FILE * file = NULL;
	char buf[512];
	init();

	switch (argc) {
	case 2:
		if (!strcmp(argv[1], "--help") || !strcmp(argv[1], "-h")) {
			printUsage();
			return 0;
		} else if (!strcmp(argv[1], "--man-es") || !strcmp(argv[1], "-e")) {
			printHelp_es();
			return 0;
		} else if (!strcmp(argv[1], "--man-en") || !strcmp(argv[1], "-m")) {
			printHelp();
			return 0;
		}
		//si no es una opción, intentar abrir como archivo
		file = fopen(argv[1], "rt");
		if (file != NULL) {
			readingFile = 1;
			yyset_in(file);
			yyparse();
			return 0;
		} else {
			sprintf(buf, "File \'%s\' cannot be opened", argv[1]);
			perror(buf);
			exit(-1);
		}
		break;
	case 1:
		printf("> ");
		yyparse();
		return 0;
		break;
	default:
		printUsage();
		exit(-2);
		break;
	}

}

void init() {
	inicializarTabla();
}

void printUsage() {
	printf("Usage:\n");
	printf("\tcalculator [filename]\n");
	printf("\nArguments:\n");
	printf("\tfilename\tfile to load\n");
	printf("Options:\n");
	printf("\t-h,--help\tPrint this help\n");
	printf("\t-m,--man-en\tPrint manual and exit (english)\n");
	printf("\t-e,--man-es\tPrint manual and exit (spanish)");
	printf(
			"\nIf no filename is provided program will start in interactive mode.\n");
}
