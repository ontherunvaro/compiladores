
%{
    #include<stdio.h>
    #include<stdlib.h>
    #include<math.h>
    #include<string.h>
    
    #include"symtab.h"
    #include"util.h"

    extern int yylex();
    extern int yyparse();
	extern FILE *yyin;
    void yyerror(const char *s);
    
    symbol auxs;
    char errmsg[512];
    extern int readingFile;
%}

%define parse.error verbose

%union{
	double fv;
	symbol sv;
}

%token <fv> NUM
%token <sv> VAR FUNC CONST
%token EXIT
%token PRINT
%token INIT
%token HELP
%token ES

%left '-' '+'
%left '*' '/' '%'
%right EXP
%left NEG

%type <fv> expr

%%
	
input:	
		| input line
		;

line:	'\n' 				{ if (!readingFile) printf("\n> ");}
		| expr '\n' 		{ if (!readingFile) printf("> ", $1);}
		| EXIT '\n' 		{ printf("Bye!\n"); YYACCEPT;}
		| PRINT expr '\n' 	{ printf("Value: %lf\n> ", $2);}
		| PRINT '\n' 		{ imprimeVariables(); printf("\n> ");}
		| INIT '\n'			{ inicializarTabla();}
		| HELP '\n'			{ printHelp(); printf("\n> ");}
		| HELP ES '\n'		{ printHelp_es(); printf("\n>");}
		| error '\n' 		{ yyerrok; printf("> ");}
		;

expr:	NUM { $$ = $1;}
		| CONST					{	$$ = $1.value.var ;}
		| VAR					{ if(isnan($1.value.var)){	
									sprintf(errmsg,"Undefined variable '%s'",$1.nombre);	
									yyerror(errmsg);
									YYERROR;
									}else
									$$ = $1.value.var; }
		| VAR '=' expr			{ $$ = $3;
								  auxs.value.var=$3;
								  auxs.tipo=VAR;
								  auxs.nombre=$1.nombre; 
								  escribeSimbolo(auxs);
								}
		| FUNC '(' expr ')'		{ 
									if((!strcmp($1.nombre,"sqrt")||!strcmp($1.nombre,"log")||!strcmp($1.nombre,"ln"))&& $3 < 0){
									 sprintf(errmsg,"%s of a negative value",$1.nombre);
									 yyerror(errmsg);
									}
									
									$$=$1.value.func($3);
								}
		| expr '+' expr 		{ $$ = $1 + $3;}
		| expr '-' expr 		{ $$ = $1 - $3;}
		| expr '*' expr 		{ $$ = $1 * $3;}
		| expr '/' expr 		{ if($3!=0)
								  $$ = $1 / $3;
								  else{
								  	yyerror("Division by zero");
								  	YYERROR;
								  }
								}
		| expr EXP expr 		{ $$ = pow($1,$3);}
		| '-' expr %prec NEG 	{ $$ = -$2;}
		| '(' expr ')' 			{ $$ = $2;}
		| expr '%' expr			{ $$ = fmod($1,$3);}
		;


%%
void yyerror(const char *s){
	printf("ERROR: %s\n",s);
	
}

