Archivos incluídos:
	src/
		alex.c
		alex.h
		asyn.c
		defs.h
		error.c
		error.h
		lex.l
		input.c
		input.h
		symtab.c
		symtab.h
		util.c
		util.h
	res/
		wilcoxon.py
	lib/
		lista.c
		lista.h
		uthash.h
	makefile
	readme.txt


Compilar desde esta carpeta con:
	make all
Ejecutar el programa con:
	./ALEX.out
Si es necesario borrar los archivos compilados, dependencias y demás:
	make clean
	
Notas para la corrección:
	- Los archivos generados por flex (lex.yy.c, lex.yy.h) son generados por make all, y
	por lo tanto pueden ser consultados sólo después de ejecutarlo
	- 