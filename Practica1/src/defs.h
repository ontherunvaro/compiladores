/*
 * defs.h
 *
 * Fichero de definiciones
 */

#ifndef DEFS_H_
#define DEFS_H_

/*
 * Los códigos se definen en este fichero.
 * Se usa este typedef para más claridad: de este
 * modo podemos saber para qué se está usando un entero.
 */
typedef int CodigoCL;

/*
 * Estructura básica de un CL. Contiene lexema y código.
 * Es devuelta por la función siguienteComponente de alex.h
 */
typedef struct CLStruct {
	char * lexema;
	CodigoCL codigo;
} CLStruct;

/*BEGIN CODES*/
/* PALABRAS RESERVADAS */
#define IMPORT 300
#define AS 301
#define DEF 302
#define FOR 303
#define IN 304
#define IF 305
#define ELIF 306
#define ELSE 307
#define NOT 308
#define PRINT 309
/***********************/
/* OTROS CL */
#define EQUAL_EQUAL 400
#define PLUS_OR_EQUAL 401
#define LESS_OR_EQUAL 402
#define STRING 404
#define MULTILINE_STRING 405
#define INTEGER 406
#define FLOAT 407
#define LINE_END 408
#define INDENTATION 409
#define IDENTIFIER 410
#define END_OF_FILE 411
#define PLUS_EQUAL 412
#define MINUS_EQUAL 413
#define PLUS_PLUS 413
#define MINUS_MINUS 414
#define POWER 415
#define LESSER_OR_EQUAL 416
#define GREATER_OR_EQUAL 417
/**********/
/* ESPECIALES (códigos de control) */
#define ERROR 5555
/**************/
/*END CODES*/

#endif /* DEFS_H_ */
