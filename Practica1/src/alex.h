/*
 * alex.h
 *
 * Cabecera del subsistema de análisis léxico
 */

#ifndef SRC_ALEX_H_
#define SRC_ALEX_H_

#include "defs.h"

/*
 * Devuelve el siguiente componente léxico.
 * Descarta todos los caracteres intermedios necesarios.
 */
CLStruct siguienteComponente();

#endif /* SRC_ALEX_H_ */
