/*
 * util.c
 *
 * Implementación de las utilidades
 */
#include "defs.h"

int lines = 1;

char * StructToString(CLStruct c) {
	switch (c.codigo) {
	case IMPORT:
		return "IMPORT";
	case AS:
		return "AS";
	case DEF:
		return "DEF";
	case FOR:
		return "FOR";
	case IN:
		return "IN";
	case IF:
		return "IF";
	case ELIF:
		return "ELIF";
	case ELSE:
		return "ELSE";
	case EQUAL_EQUAL:
		return "EQUAL_EQUAL";
	case PLUS_OR_EQUAL:
		return "PLUS_OR_EQUAL";
	case LESS_OR_EQUAL:
		return "LESS_OR_EQUAL";
	case MULTILINE_STRING:
		return "MULTILINE_STRING";
	case STRING:
		return "STRING";
	case INTEGER:
		return "INTEGER";
	case FLOAT:
		return "FLOAT";
	case LINE_END:
		return "LINE_END";
	case INDENTATION:
		return "INDENTATION";
	case END_OF_FILE:
		return "END_OF_FILE";
	case IDENTIFIER:
		return "IDENTIFIER";
	case ERROR:
		return "ERROR";
	case NOT:
		return "NOT";
	case PRINT:
		return "PRINT";
	default:
		/*
		 * Si el tipo de lexema no está definico en la lista de arriba,
		 * es porque es un símbolo. En ese caso se usa el propio símbolo
		 * como identificador del tipo.
		 */
		return c.lexema;
	}
	return "UNDEFINED";
}

void addLine() {
	lines++;
}

void addLines(int n) {
	lines += n;
}

int getLineCount() {
	return lines;
}
