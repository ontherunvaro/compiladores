%option noyywrap

%{
#include "defs.h"
#include "error.h"
#include "util.h"
%}

DIG [[:digit:]]
HDIG [[:xdigit:]]
ID [_[:alpha:]][_[:alnum:]]*
ESC_SEQ \\.
SIN_STR \'({ESC_SEQ}|[^\\\n\'])*\'
DBL_STR \"({ESC_SEQ}|[^\\\n\"])*\"
SIN_SYMB [\+\-\*\/\.\,\:\=\<\>]

%x IN_MULTILINE

	int parens=0;
	int brackets=0;
	int braces=0;
%%
	int partial_line_count=0;
	

<INITIAL>{


	/*Comentarios*/
#[^\n]*$		;


	/*Identificadores y palabras reservadas*/
import		return IMPORT;
as			return AS;
def			return DEF;
for			return FOR;
in			return IN;
if			return IF;
elif		return ELIF;
else		return ELSE;
not			return NOT;
print		return PRINT;
{ID}		return IDENTIFIER;


	/*Strings*/
\"{3}							{ yymore(); BEGIN(IN_MULTILINE);}
{SIN_STR}|{DBL_STR}				return STRING;
\"[^\"\n]*$						{generarError(UNCLOSED_STRING,0,NULL); parens=0;brackets=0;braces=0;}
\'[^\']*$						{generarError(UNCLOSED_STRING,0,NULL); parens=0;brackets=0;braces=0;}

	/*Numeros*/
0[xX]{HDIG}+						return INTEGER;
{DIG}+							return INTEGER;

{DIG}+\.{DIG}*					return FLOAT;
\.{DIG}+						return FLOAT;
{DIG}+(\.{DIG}+)?[eE][\-\+]?{DIG}+		return FLOAT;


	/*Simbolos*/
{SIN_SYMB}	return yytext[0];
\=\=		return EQUAL_EQUAL;
\>\=		return PLUS_OR_EQUAL;
\<\=		return LESS_OR_EQUAL;
\+\+		return PLUS_PLUS;
\-\-		return MINUS_MINUS;
\+\=		return PLUS_EQUAL;
\-\=		return MINUS_EQUAL;
\*\*		return POWER;
\{			{braces++; return '{';}
\}			{braces--; return '}';}
\(			{parens++; return '(';}
\)			{parens--; return ')';}
\[			{brackets++; return '[';}
\]			{brackets--; return ']';}


	/*Especiales*/
<<EOF>>		return END_OF_FILE;
\n			{ addLine(); 
				if(!(braces || parens || brackets)){
				return LINE_END;}
				}
^\ +		{ if(!(braces || parens || brackets)) return INDENTATION;}


	/*No reconocidos*/
[[:space:]]		/*no hacer nada*/	
.			generarError(UNRECOGNIZED_CHAR,yytext[0],NULL);
 

}

<IN_MULTILINE>{
\n			{yymore(); partial_line_count++;}
\"{3}		{ BEGIN(INITIAL); addLines(partial_line_count); partial_line_count=0; return MULTILINE_STRING; }
<<EOF>>		{generarError(UNCLOSED_STRING, 0, NULL); yyterminate();}
.			yymore(); /*cualquier cosa*/
}


