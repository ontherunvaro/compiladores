/*
 * alex.c
 *
 * Wrapper del analizador léxico.
 * Empaqueta los lexemas y los códigos e inserta en la tabla de símbolos
 */
#include "defs.h"
#include "lex.yy.h"
#include "symtab.h"
#include "util.h"

/*
 * Genera una estructura a partir de un código de CL y un lexema
 */
CLStruct genStruct(CodigoCL code, char* lex) {
	CLStruct ret;
	ret.lexema = lex;
	ret.codigo = code;
	return ret;
}

CLStruct siguienteComponente() {
	CodigoCL code = yylex();
	if (code == IDENTIFIER) {
		insercionCondicional(yytext, getLineCount());
	}
	return genStruct(code, yytext);
}
