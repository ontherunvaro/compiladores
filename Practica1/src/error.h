/*
 * error.h
 *
 *
 *  Cabecera del subsistema de gestión de errores.
 */

#ifndef SRC_ERROR_H_
#define SRC_ERROR_H_

/*
 * Definición de tipo para mayor claridad al usar un int como código.
 */
typedef int CodigoError;

/*BEGIN ERRORCODES*/
#define UNRECOGNIZED_CHAR 900
#define FILE_NOT_FOUND 901
#define LEXEM_TOO_LONG 902
#define UNCLOSED_STRING 903
/*END ERRORCODES*/

/*
 * Función que genera los errores. El único parámetro obligatorio
 * es el código.
 */
void generarError(CodigoError code, char last, char * additional_info);

#endif /* SRC_ERROR_H_ */
