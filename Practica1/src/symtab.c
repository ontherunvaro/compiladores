/*
 * symtab.c
 *
 *  Implementación de la tabla de símbolos
 */

#include "symtab.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../lib/lista.h"
#include "../lib/uthash.h"
#include "defs.h"
#include "util.h"

/*
 * Número de línea ficticio para las palabras reservadas
 */
#define RESERVED -8888

/*
 * Estructura que representa cada celda de la tabla
 */
typedef struct {
	char * lexema;
	CodigoCL codigo;
	LISTA lineas;		//Lista de lineas en las que aparece el lexema
	UT_hash_handle hh; //campo manejador de la tabla hash, propio de la implementacion
} Celda;

/*
 * Cabecera de la tabla hash.
 * La implementación requiere su inicialización a NULL
 */
Celda *tabla = NULL;

/*
 * Añade un elemento a la tabla hash
 */
void add(char * lex, CodigoCL codigo, int linea) {
	Celda *s;
	char * c;

	s = (Celda *) malloc(sizeof(Celda));
	c = strdup(lex);

	s->lexema = c;
	s->codigo = codigo;

	if (!existe(s->lineas)) {
		crea(&s->lineas);
	}
	if (linea != RESERVED) {
		inserta(&s->lineas, primero(s->lineas), linea);
	}

	HASH_ADD_KEYPTR(hh, tabla, s->lexema, strlen(s->lexema), s);
}

/*
 * Busca un elemento en la tabla hash
 */
Celda * find(char * lex) {
	Celda *s;
	HASH_FIND_STR(tabla, lex, s);
	return s;
}

/*
 * Añade una línea a un elemento
 */
void addNewLine(Celda *s, int linea) {
	int lastline;
	if (esvacia(s->lineas)) {
		inserta(&s->lineas, primero(s->lineas), linea);
	} else {
		recupera(s->lineas, anterior(s->lineas, fin(s->lineas)), &lastline);
		if (lastline < linea) {
			inserta(&s->lineas, fin(s->lineas), linea);
			HASH_DELETE(hh, tabla, s);
			HASH_ADD_KEYPTR(hh, tabla, s->lexema, strlen(s->lexema), s);
			/*
			 * Esta implementación de la tabla hash no permite modificar un elemento
			 * directamente, hay que eliminarlo y luego insertarlo tras modificarlo.
			 */
		}

	}

}

CodigoCL insercionCondicional(char * lex, int linea) {
	Celda *s;
	s = find(lex);
	if (s != NULL) {
		addNewLine(s, linea);
		return s->codigo;
	} else {
		add(lex, IDENTIFIER, linea);
		return IDENTIFIER;
	}
}

CLStruct buscar(char *lexema) {
	Celda *s = find(lexema);
	CLStruct ret;
	if (s != NULL) {
		ret.lexema = (char *) malloc((strlen(lexema) + 1) * sizeof(char));
		strcpy(ret.lexema, lexema);
		ret.codigo = s->codigo;
	} else {
		ret.codigo = ERROR;
	}
	return ret;
}

/*
 * Funcion auxiliar para ordenar la tabla hash
 */
int name_sort(Celda *a, Celda *b) {
	return strcmp(a->lexema, b->lexema);
}

void imprimirTabla() {
	Celda *s, *tmp;
	tipoelem e;
	posicion p;
	CLStruct c; //para poder usar StructToString
	int i = 0, k = 0;

	/*
	 * Se ordena la tabla por lexema por pura estética.
	 * Como tabla hash, el acceso siempre es o(1), y
	 * es independiente del orden en el que estén almacenados
	 * los elementos.
	 */
	HASH_SORT(tabla, name_sort);

	printf(
			"\n\n---------------------------TABLA DE SIMBOLOS----------------------------\n");
	printf("\n %-20s%-10s  %-20s", "Lexema", "Tipo", "Lineas");

	printf("\n");
	for (i = 0; i < 79; i++) {
		printf("-");
	}
	HASH_ITER(hh, tabla, s, tmp)
	//itera sobre la tabla
	{
		c.codigo = s->codigo;
		c.lexema = s->lexema;
		printf("\n %-20s%-10s", s->lexema, StructToString(c));

		p = primero(s->lineas);
		recupera(s->lineas, p, &e);
		printf("  %d", e);
		for (k = 1; k < longitud(s->lineas); k++) {
			p = siguiente(s->lineas, p);
			recupera(s->lineas, p, &e);
			printf(",%d", e);

		}
	}
	printf("\n");
	for (i = 0; i < 79; i++) {
		printf("-");
	}
}

void inicializarTabla() {

	add("import", IMPORT, RESERVED);
	add("as", AS, RESERVED);
	add("def", DEF, RESERVED);
	add("for", FOR, RESERVED);
	add("in", IN, RESERVED);
	add("if", IF, RESERVED);
	add("elif", ELIF, RESERVED);
	add("else", ELSE, RESERVED);
	add("not", NOT, RESERVED);
	add("print", PRINT, RESERVED);

}
