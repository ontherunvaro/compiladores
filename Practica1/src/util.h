/*
 * util.h
 *
 * Fichero de utilidades comunes
 *
 * El manejo de líneas se lleva a cabo en este fichero
 * para que sea accesible desde las distintas partes del programa.
 */

#ifndef SRC_UTIL_H_
#define SRC_UTIL_H_

#include "defs.h"

/*
 * Devuelve linea actual
 */
int getLineCount();

/*
 * Añade una linea al contador
 */
void addLine();

/*
 * Añade n lineas al contador
 */
void addLines(int n);

/*
 * Devuelve la representación string del tipo de un componente léxico.
 * Si el CL es un solo caracter, la representacion string será ese caracter.
 */
char * StructToString(CLStruct c);

#endif /* SRC_UTIL_H_ */
