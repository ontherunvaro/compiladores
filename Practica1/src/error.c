/*
 * error.c
 *
 * Implementación del subsistema de gestión de errores
 */

#include "error.h"

#include <stdio.h>
#include <stdlib.h>

#include "util.h"

void generarError(CodigoError code, char last, char * additional_info) {
	int ex = 0;

	switch (code) {
	case UNRECOGNIZED_CHAR:
		printf("\nERROR: Illegal character \'%c\' near line %d\n", last,
				getLineCount());
		break;
	case LEXEM_TOO_LONG:
		printf("\nERROR: Name too long near line %d\n", getLineCount());
		break;
	case FILE_NOT_FOUND:
		printf("\nCRITICAL: Couldn't access source file %s\n", additional_info);
		ex = 1; //No encontrar el archivo es un error crítico: se para el análisis.
		break;
	case UNCLOSED_STRING:
		printf("\nERROR: Unclosed string near line %d\n", getLineCount());
		break;
	}

	if (ex) {
		printf("\nCOMPILATION TERMINATED, EXIT STATUS %d", code);
		fflush (stdout);
		exit(code);
	}
}
