/*
 * asyn.c
 *
 * Fichero principal de ejecución del programa.
 * Simula un analizador sintáctico.
 */

#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include "symtab.h"
#include "util.h"

#include "lex.yy.h"
#include "alex.h"
#define ARCHIVO "res/wilcoxon.py"

/*
 * Función principal.
 * 	-Pide e imprime cada componente léxico del fichero
 * 	-Imprime el número de CL leídos
 * 	-Imprime la tabla de símbolos
 */
int main(void) {
	CLStruct c;
	int i = 0;

	FILE * pt;

	pt = fopen(ARCHIVO, "rt");
	if (pt != NULL) {
		yyset_in(pt);
		while (c.codigo != END_OF_FILE) {
			c = siguienteComponente();
			printf("START %d\n", i + 1);
			printf("\tTYPE %s\n", StructToString(c));
			printf("\tLEX #%s#\n", c.lexema);
			//Se utiliza '#' como delimitador de los lexemas
			fflush (stdout);
			i++;
		}
		printf("\nEND: Read = %d\n", i);
		imprimirTabla();
		printf("\n\nEND PROGRAM\n");
		fflush (stdout);
		fclose(pt);

	} else
		return 1;
	return 0;
}

