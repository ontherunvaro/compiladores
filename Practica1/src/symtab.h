/*
 * symtab.h
 *
 *  Cabecera de la tabla de símbolos
 */

#ifndef SRC_SYMTAB_H_
#define SRC_SYMTAB_H_

#include "defs.h"

/*
 * Busca en la tabla de simbolos el lexema.
 * Si el lexema existe en la tabla, devuelve el tipo y termina.
 * Si el lexema no existe en la tabla, inserta y devuelve el tipo.
 */
CodigoCL insercionCondicional(char * lexema, int linea);

/*
 * Busca en la tabla de símbolos el lexema.
 * Devuelve un CLStruct conteniendo el componente léxico y el lexema.
 * Si no existe en la tabla, devuelve NULL.
 */
CLStruct buscar(char *lexema);

/*
 * Carga las palabras reservadas en la tabla.
 */
void inicializarTabla();

/*
 * Imprime la tabla de símbolos
 */
void imprimirTabla();

#endif /* SRC_SYMTAB_H_ */
