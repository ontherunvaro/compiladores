#include <stdlib.h>
#include "lista.h"

void crea(LISTA *l) {
	(*l) = (LISTA) malloc(sizeof(struct l));
	(*l)->inicio = (posicion) malloc(sizeof(struct celda));
	(*l)->fin = (*l)->inicio;
	((*l)->fin)->sig = NULL;
	(*l)->longitud = 0;
}

void destruye(LISTA *l) {
	(*l)->fin = (*l)->inicio;
	while ((*l)->fin != NULL) {
		(*l)->fin = ((*l)->fin)->sig;
		free((*l)->inicio);
		(*l)->inicio = (*l)->fin;
	}
	free(*l);
	*l = NULL;

}

posicion primero(LISTA l) {
	return (l->inicio);
}

posicion siguiente(LISTA l, posicion p) {
	return (p->sig);
}

posicion fin(LISTA l) {
	return (l->fin);
}

posicion anterior(LISTA l, posicion p) {
	posicion q;

	q = l->inicio;

	while (q->sig != p) {
		q = q->sig;
	}
	return q;

}

unsigned existe(LISTA l) {
	if (l != NULL)
		return 1;
	return 0;

}

unsigned esvacia(LISTA l) {
	if (l->longitud == 0)
		return 1;
	return 0;

}

void recupera(LISTA l, posicion p, tipoelem *e) {
	*e = (p->sig)->elemento;
}

unsigned longitud(LISTA l) {
	return (l->longitud);
}

void inserta(LISTA *l, posicion p, tipoelem e) {
	posicion q;

	q = p->sig;

	p->sig = (posicion) malloc(sizeof(struct celda));
	(p->sig)->elemento = e;
	(p->sig)->sig = q;
	if (q == NULL)
		(*l)->fin = p->sig;
	(*l)->longitud = (*l)->longitud + 1;

}

void suprime(LISTA *l, posicion p) {
	posicion q;

	q = p->sig;
	p->sig = q->sig;
	if (p->sig == NULL)
		(*l)->fin = p;
	free(q);
	(*l)->longitud = (*l)->longitud - 1;
}

void modifica(LISTA *l, posicion p, tipoelem e) {
	(p->sig)->elemento = e;
}

