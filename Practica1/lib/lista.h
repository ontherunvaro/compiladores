#define N 3

typedef int tipoelem;

struct celda {
	tipoelem elemento;
	struct celda *sig;
};

typedef struct celda * posicion;

struct l {

	posicion inicio;
	unsigned longitud;
	posicion fin;
};

typedef struct l * LISTA;

void crea(LISTA *l);
void destruye(LISTA *l);
posicion primero(LISTA l);
posicion siguiente(LISTA l, posicion p);
posicion fin(LISTA l);
posicion anterior(LISTA l, posicion p);
unsigned existe(LISTA l);
unsigned esvacia(LISTA l);
void recupera(LISTA l, posicion p, tipoelem *e);
unsigned longitud(LISTA l);
void inserta(LISTA *l, posicion p, tipoelem e);
void suprime(LISTA *l, posicion p);
void modifica(LISTA *l, posicion p, tipoelem e);
posicion posinser(LISTA l, tipoelem e);
