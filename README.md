# Compiladores e Intérpretes #
Repositorio creado para las prácticas de la asignatura Compiladores e Intérpretes en el curso 2015/2016.

## Proyectos ##
### Practica1 ###
Analizador léxico para Python escrito en C

### Practica3 ###
Calculadora científica en flex/bison




*2015/16 Álvaro Brey Vilas. Todos los derechos reservados*